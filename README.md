# Ansible-Rollen

Meine selbst geschriebenen Ansible-Rollen:

- [local-deb-repo](local-deb-repo/README.md)  Lokales Debian Repro fuer Software, wo nur es nur debs gibt
- [export_nextcloud](export_nextcloud/README.md) Export address book and calendar entries from a Nextcloud.
- [starship](starship/README.md) Installs `starship` with a configuration. 
