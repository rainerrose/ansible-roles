#!/bin/bash
cd "{{ PACKAGES_HTDOCS }}"

######### Threema
wget -q https://releases.threema.ch/web-electron/v1/release/Threema-Latest.deb
version=$(dpkg-deb -W Threema-Latest.deb  | cut -f 2)
mv -f Threema-Latest.deb Threema-$version.deb
test -e Threema-Latest.deb && rm Threema-Latest.deb

######## Minecraft
 
wget -q -O minecraft-Latest.deb 'https://launcher.mojang.com/download/Minecraft.deb'
version=$(dpkg-deb -W minecraft-Latest.deb  | cut -f 2)
mv -f minecraft-Latest.deb minecraft-$version.deb
test -e minecraft-Latest.deb && rm minecraft-Latest.deb

######## Discord
wget -q -O Discord-Latest.deb "https://discord.com/api/download?platform=linux&format=deb" 
version=$(dpkg-deb -W Discord-Latest.deb | cut -f 2)
mv -f Discord-Latest.deb Discord-$version.deb
test -e Discord-Latest.deb && rm Discord-Latest.deb

####### Repo neu aufbauen
dpkg-scanpackages --multiversion ./ > Packages 2>/dev/null && gzip -k -f -9 Packages
apt-ftparchive release . > Release
