local-deb-repo
==============

Lokales Debian Repro fuer Software, wo nur es nur debs gibt

`sources-list` wird kopiert in das Repo-Verzeichnis, damit man diese Datei leichter und vorausgefüllt nach `/etc/apt/sources.list.d` auf das lokale System kopieren kann.

Unterhalb des Webserver-Verzeichnisses (Variable `HTDOCSDIR`) einfach alle `.deb`-Dateien kopieren.

Danach `update_repo.sh` aufrufen. Automatisch heruntergeladen wird derzeit

- Threema
- Minecraft
- Discord

Nach dem Kopieren der o.g. `sources-list` dann 

~~~ bash
sudo apt update
sudo apt list --upgradeable
sudo apt full-upgrade
~~~

ausführen und es sollten die neuen Pakete angezeigt werden, bzw über ein 

~~~ bash
apt-cache search Threema
~~~

dann auffindbar und installierbar sein.

Requirements
------------

Laufender Webserver, der Symlinks folgt.

Role Variables
--------------

Guckst Du `roles/local-deb-repo/vars/main.yml' sollte selbsterklärend sein

Dependencies
------------

Example Playbook
----------------
siehe [play.yml](play.yml)

License
-------

GPLv2

Author Information
------------------

Rainer Rose
