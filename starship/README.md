# Role starship

Installs `starship` with a configuration. Configuration-Parameters see [here](https://starship.rs/config/).

Developed for and under Manjaro. Prepared for Debian but **not tested**.

## Requirements

Not so far.

## Role Variables


| Variable                 | Default | Description                                                                 |
| ------------------------ | ------- | --------------------------------------------------------------------------- |
| `starship_user`          | `[]`    | A List of users where starship should be installed.                         |
| `starship_extra_options` | `Empty` | Additional configuration-parameters see [here](https://starship.rs/config/) |

## Dependencies

Not so far.

## Example Playbook

~~~yaml
---

- name: Setup starship
  hosts: linuxhosts
  roles:
    - starship
  vars:
    starship_user:
      - user1
      - user2
~~~

or see [play.yml](play.yml).

## Author Information

Rainer Rose
