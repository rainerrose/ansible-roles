# Role: export_nextcloud

Export address book and calendar entries from a Nextcloud.

## Features
- Create for each configured Nextcloud-User an systemd timer-unit.
- Writes Prometheus statistics in `.prom` files.
- Creates `.ics` and `.vcf`-Dumps of each configured user.

The `.ics` and `.vcf`-Files can be backup up with your favorite backup-Tool.

## systemd timer-unit
Each configured Nextcloud-User get an systemd timer-unit.

- `export_nextcloud-<nextcloud-User>.timer`

## Prometheus-Stats are written in 

`{{ export_nextcloud_prometheus_path }}/export_nextcloud_<nextcloud-User>.prom`

~~~
# HELP export_nextcloud_calendars_failed 1=failed 0=successful
# TYPE export_nextcloud_calendars_failed gauge
export_nextcloud_calendars_failed{username="user1"} 0
# HELP export_nextcloud_addressbooks_failed 1=failed 0=successful
# TYPE export_nextcloud_addressbooks_failed gauge
export_nextcloud_addressbooks_failed{username="user1"} 0
~~~

## Requirements

Nothing so far.

## Role Variables

### required

#### `export_nextcloud_user`

List of Credentials and Timestamps.

Example:

~~~yaml
export_nextcloud_user:
  - username: "user1"
    password: "password1"
    on_calendar: '*-*-* 03:00'
  - username: "user2"
    password: "password2"
    on_calendar: '*-*-* 03:01'
~~~

Instead of the password, I recommend generating a personal access token (PAT).

The syntax of the attribute `on_calendar` follows [systemd.unit(5)](https://www.freedesktop.org/software/systemd/man/systemd.timer#OnCalendar=) because it's used to generate Timer-Units for each `username`.

Pattern:

- `export_nextcloud-user1.timer`
- `export_nextcloud-user2.timer`

#### `export_nextcloud_url`

The URL of the nextcloud-instance.

Example:
~~~yaml
export_nextcloud_url: "https://example.com"
~~~

### optional


| Name Of Variable                   | Default-Value              | Description                                                             |
| ---------------------------------- | -------------------------- | ----------------------------------------------------------------------- |
| `export_nextcloud_script_path`     | `/opt/export_nextcloud`    | Folder where the script will be installed to                            |
| `export_nextcloud_backup_path`     | `/backup/nextcloud_export` | Folder where the export should be written to                            |
| `export_nextcloud_prometheus_path` | `/var/lib/node_exporter`   | Folder where the prometheus statistics are to be written (textexporter) |


## Dependencies

Not so far.

## Example Playbook

Siehe [play.yml](play.yml).


## Author Information

Rainer Rose

## Credits

https://makesmart.net/blog/read/nextcloud-backup-kontakte-und-kalender-sichern
